async function panda(id,data){
const Datastore = require('@google-cloud/datastore');

// Your Google Cloud Platform project ID
const projectId = 'erudite-carving-181801';

// Instantiates a client
const datastore = Datastore({
  projectId: projectId,
  // key:'./API Project-5a04e7b0e858.json'
});

// The kind for the new entity
const kind = 'Task';
// The name/ID for the new entity
const name = id;
// The Cloud Datastore key for the new entity
const taskKey = datastore.key([kind, name]);

// Prepares the new entity
const task = {
  key: taskKey,
  data: data
};

// Saves the entity
var success=await datastore.save(task)
return success
  
}
module.exports=panda