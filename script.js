/**
 * Responds to any HTTP request that can provide a "message" field in the body.
 *
 * @param {!Object} req Cloud Function request context.
 * @param {!Object} res Cloud Function response context.
 */
async function siacargo(number) {
const puppeteer = require('puppeteer');
  const browser = await puppeteer.launch({
    headless: true,
    timeout: 90000,
    args: ['--no-sandbox', '--disable-setuid-sandbox']
  });
  const page = await browser.newPage();
  
  await page.goto('http://www.siacargo.com/ccn/ShipmentTrack.aspx');
  // await page.waitFor(2000)
  await page.click("#Suffix1");
  // await page.waitFor(2000)
  await page.type("#Suffix1",number);
  // await page.waitFor(2000)
  // await page.click("#Suffix2");
  // await page.type("49664790");
  await page.click("#btnQuery")
  await page.waitFor(3000)
  try{
    var objek={}
    let awb = await page.evaluate(() => document.querySelectorAll('table')[1].children[0].children[0].innerText);
    let origin = await page.evaluate(() =>document.querySelectorAll('table')[1].children[0].children[1].children[0].innerText)
    let total_pcs = await page.evaluate(() =>document.querySelectorAll('table')[1].children[0].children[1].children[1].innerText)
    let destination = await page.evaluate(() =>document.querySelectorAll('table')[1].children[0].children[2].children[0].innerText)
    let total_wgt = await page.evaluate(() =>document.querySelectorAll('table')[1].children[0].children[2].children[1].innerText)
    objek.website="http://www.siacargo.com/"
    objek.awb=awb.split(":")[1].trim()
    objek.origin=origin.split(":")[1].trim()
    objek.destination=destination.split(":")[1].trim()
    objek.total_pcs=total_pcs.split(":")[1].trim()
    objek.total_wgt=total_wgt.split(":")[1].trim()
    
let booking_info = await page.evaluate(() =>{
    let booking_header=[ 'uplift_point','discharge_point','flight_no','scheduled_departure','scheduled_arrival','booking_status' ]
      var heder=[]
      var cam=document.querySelectorAll(".result-row")
      for(let i=0;i<cam.length;i++){
        var kamen={}
        let nama=document.querySelectorAll(".result-row")[i].children[5].innerText
        let kamu=document.querySelectorAll(".result-row")[i].children
        if(nama.toLowerCase().trim()=="confirmed"){
          kamen[booking_header[0]]=kamu[0].innerText
          kamen[booking_header[1]]=kamu[1].innerText
          kamen[booking_header[2]]=kamu[2].innerText
          kamen[booking_header[3]]=kamu[3].innerText
          kamen[booking_header[4]]=kamu[4].innerText
          kamen[booking_header[5]]=kamu[5].innerText
      heder.push(kamen)
        }
      }
      return heder
    })
    
    let shipment_info = await page.evaluate(() =>{
    let shipment_header=[ 'station','flight_no','status', 'date', 'time', 'pcs',  'wgt','uld_battery_temp' ]
      var heder=[]
      var cam=document.querySelectorAll(".result-row")
      for(let i=0;i<cam.length;i++){
        var kamen={}
        let nama=document.querySelectorAll(".result-row")[i].children[5].innerText
        let kamu=document.querySelectorAll(".result-row")[i].children
        if(nama.toLowerCase().trim()!="confirmed"){
          kamen[shipment_header[0]]=kamu[0].innerText
          kamen[shipment_header[1]]=kamu[1].innerText
          kamen[shipment_header[2]]=kamu[2].innerText
          kamen[shipment_header[3]]=kamu[3].innerText
          kamen[shipment_header[4]]=kamu[4].innerText
          kamen[shipment_header[5]]=kamu[5].innerText
          kamen[shipment_header[6]]=kamu[6].innerText
          kamen[shipment_header[7]]=kamu[7].innerText
          
      heder.push(kamen)
        }
      }
      return heder
    })
    objek.booking_information=booking_info
    objek.shipment_status=shipment_info
    
    var parampa=await store(objek.awb,objek)
    console.log(JSON.stringify(parampa))
    await browser.close();
    return objek
    
  }catch(err){
    await browser.close()
  return {error:"error your tracking number not recognizable"}
    
  }
  
}
async function cathaypacific(number) {
const puppeteer = require('puppeteer');
  const browser = await puppeteer.launch({
    headless: true,
    timeout: 90000,
    args: ['--no-sandbox', '--disable-setuid-sandbox']
  });
  const page = await browser.newPage();
  
  await page.goto(`http://www.cathaypacificcargo.com/ManageYourShipment/TrackYourShipment/tabid/108/SingleAWBNo/160-${number}/language/en-US/Default.aspx`);
  // await page.waitFor(2000)
  await page.click("#dnn_ctr779_ViewTnT_ctl00_hrefShowBookingInfo");
  await page.waitFor(3000)
  await page.click("#dnn_ctr779_ViewTnT_ctl00_lbtnShowAllShipmentStatus");
  await page.waitFor(3000)
  try{
    var objek={}
    let awb = await page.evaluate(() => document.querySelectorAll('#dnn_ctr779_ViewTnT_ctl00_lblTnTSingleAWBNo')[0].innerText);
    let origin = await page.evaluate(() =>document.querySelectorAll('.table_color_odd')[0].children[0].innerText)
    let total_pcs = await page.evaluate(() =>document.querySelectorAll('.table_color_odd')[0].children[2].innerText)
    let destination = await page.evaluate(() =>document.querySelectorAll('.table_color_odd')[0].children[1].innerText)
    let total_wgt = await page.evaluate(() =>document.querySelectorAll('.table_color_odd')[0].children[3].innerText)
    objek.website="http://www.cathaypacificcargo.com/"
    objek.awb=awb
    objek.origin=origin
    objek.destination=destination
    objek.total_pcs=total_pcs
    objek.total_wgt=total_wgt
    
let booking_info = await page.evaluate(() =>{
    let booking_header=[ 'origin','destination','flight_no','depart_on','arrival_on','pieces','weight','status' ]
      var heder=[]
      var cam=document.querySelectorAll('.general_table')[1].children[0].children
      for(let i=1;i<cam.length;i++){
        var kamen={}
        var kamu=document.querySelectorAll('.general_table')[1].children[0].children[i].children
          kamen[booking_header[0]]=kamu[0].innerText
          kamen[booking_header[1]]=kamu[1].innerText
          kamen[booking_header[2]]=kamu[2].innerText
          kamen[booking_header[3]]=kamu[3].children[0].children[0].innerText
          kamen[booking_header[4]]=kamu[4].children[0].children[0].innerText
          kamen[booking_header[5]]=kamu[5].innerText
          kamen[booking_header[6]]=kamu[6].innerText
          kamen[booking_header[7]]=kamu[7].innerText
      heder.push(kamen)
        
      }
      return heder
    })
    
    let shipment_info = await page.evaluate(() =>{
    let shipment_header=[ 'no','status','port', 'flight', 'event_time', 'pieces',  'weight','remarks' ]
      var heder=[]
      var cam=document.querySelectorAll('.general_table')[2].children[0].children
      for(let i=1;i<cam.length;i++){
        var kamen={}
        let kamu=document.querySelectorAll('.general_table')[2].children[0].children[i].children
          kamen[shipment_header[0]]=kamu[0].innerText
          kamen[shipment_header[1]]=kamu[1].innerText
          kamen[shipment_header[2]]=kamu[2].innerText
          kamen[shipment_header[3]]=kamu[3].innerText
          kamen[shipment_header[4]]=kamu[4].innerText
          kamen[shipment_header[5]]=kamu[5].innerText
          kamen[shipment_header[6]]=kamu[6].innerText
          kamen[shipment_header[7]]=kamu[7].innerText
          
      heder.push(kamen)
        
      }
      return heder
    })
    objek.booking_information=booking_info
    objek.shipment_status=shipment_info
    
    var parampa=await store(objek.awb,objek)
    console.log(JSON.stringify(parampa))
   await browser.close();
   return objek
    
  }catch(err){
await browser.close()
    return {error:"error your tracking number not recognizable"}

  }
  
}

async function store(id,data){
const Datastore = require('@google-cloud/datastore');

// Your Google Cloud Platform project ID
const projectId = 'erudite-carving-181801';

// Instantiates a client
const datastore = Datastore({
  projectId: projectId,
  // key:'./API Project-5a04e7b0e858.json'
});

// The kind for the new entity
const kind = 'Task';
// The name/ID for the new entity
const name = id;
// The Cloud Datastore key for the new entity
const taskKey = datastore.key([kind, name]);

// Prepares the new entity
const task = {
  key: taskKey,
  data: data
};

// Saves the entity
var success=await datastore.save(task)
return success
  
}
exports.helloWorld = function helloWorld(req, res) {
  if(req.body.tracking.substring(0,3)=="618"){
        
        res.send(siacargo(req.body.tracking.split("-")[1].trim()))
    }else if(req.body.tracking.substring(0,3)=="160"){
        
        res.send(cathaypacific(req.body.tracking.split("-")[1].trim()))
    }else{
        res.json({error:"only use tracking number 618 for siacargo and 160 for cathaypacific"})
    }
};
