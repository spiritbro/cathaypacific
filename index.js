const express = require('express')
const app = express()
const bodyParser=require('body-parser')
const mongoose=require('mongoose')
// mongoose.connect('mongodb://localhost/test');
// var Person = mongoose.model('Person', yourSchema);
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.get('/', (req, res) => res.send(`<form action="/tracking" method="post">
 example:160-53490990 for cathaypacific , 618-49664790 for siacargo<br>
  Input your tracking number: <input type="text" name="tracking"><br>
  <input type="submit" value="Submit">
</form>`))
app.post('/tracking',async (req, res) =>{
    if(req.body.tracking.substring(0,3)=="618"){
        const siacargo=require('./siacargo')
        res.send(await siacargo(req.body.tracking.split("-")[1].trim()))
    }else if(req.body.tracking.substring(0,3)=="160"){
        const cathaypacific=require('./cathaypacific')
        res.send(await cathaypacific(req.body.tracking.split("-")[1].trim()))
    }else{
        res.json({error:"only use tracking number 618 for siacargo and 160 for cathaypacific"})
    }
})
app.get('/tracking',async (req, res) =>{
    
    if(req.query.tracking.substring(0,3)=="618"){
        const siacargo=require('./siacargo')
        res.send(await siacargo(req.query.tracking.split("-")[1].trim()))
    }else if(req.query.tracking.substring(0,3)=="160"){
        const cathaypacific=require('./cathaypacific')
        res.send(await cathaypacific(req.query.tracking.split("-")[1].trim()))
    }else{
        res.json({error:"only use tracking number 618 for siacargo and 160 for cathaypacific"})
    }
})

app.listen(process.env.PORT, () => console.log('Example app listening on port 3000!'))