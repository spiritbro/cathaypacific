

async function siacargo(number) {
const puppeteer = require('puppeteer');
  const browser = await puppeteer.launch({
    executablePath:__dirname+"/headless_shell",
    headless: true,
    timeout: 90000,
    args: ['--no-sandbox', '--disable-setuid-sandbox']
  });
  const page = await browser.newPage();
  
  await page.goto('http://www.siacargo.com/ccn/ShipmentTrack.aspx');
  // await page.waitFor(2000)
  await page.click("#Suffix1");
  // await page.waitFor(2000)
  await page.type("#Suffix1",number);
  // await page.waitFor(2000)
  // await page.click("#Suffix2");
  // await page.type("49664790");
  await page.click("#btnQuery")
  await page.waitFor(3000)
  try{
    var objek={}
    let awb = await page.evaluate(() => document.querySelectorAll('table')[1].children[0].children[0].innerText);
    let origin = await page.evaluate(() =>document.querySelectorAll('table')[1].children[0].children[1].children[0].innerText)
    let total_pcs = await page.evaluate(() =>document.querySelectorAll('table')[1].children[0].children[1].children[1].innerText)
    let destination = await page.evaluate(() =>document.querySelectorAll('table')[1].children[0].children[2].children[0].innerText)
    let total_wgt = await page.evaluate(() =>document.querySelectorAll('table')[1].children[0].children[2].children[1].innerText)
    objek.website="http://www.siacargo.com/"
    objek.awb=awb.split(":")[1].trim()
    objek.origin=origin.split(":")[1].trim()
    objek.destination=destination.split(":")[1].trim()
    objek.total_pcs=total_pcs.split(":")[1].trim()
    objek.total_wgt=total_wgt.split(":")[1].trim()
    
let booking_info = await page.evaluate(() =>{
    let booking_header=[ 'uplift_point','discharge_point','flight_no','scheduled_departure','scheduled_arrival','booking_status' ]
      var heder=[]
      var cam=document.querySelectorAll(".result-row")
      for(let i=0;i<cam.length;i++){
        var kamen={}
        let nama=document.querySelectorAll(".result-row")[i].children[5].innerText
        let kamu=document.querySelectorAll(".result-row")[i].children
        if(nama.toLowerCase().trim()=="confirmed"){
          kamen[booking_header[0]]=kamu[0].innerText
          kamen[booking_header[1]]=kamu[1].innerText
          kamen[booking_header[2]]=kamu[2].innerText
          kamen[booking_header[3]]=kamu[3].innerText
          kamen[booking_header[4]]=kamu[4].innerText
          kamen[booking_header[5]]=kamu[5].innerText
      heder.push(kamen)
        }
      }
      return heder
    })
    
    let shipment_info = await page.evaluate(() =>{
    let shipment_header=[ 'station','flight_no','status', 'date', 'time', 'pcs',  'wgt','uld_battery_temp' ]
      var heder=[]
      var cam=document.querySelectorAll(".result-row")
      for(let i=0;i<cam.length;i++){
        var kamen={}
        let nama=document.querySelectorAll(".result-row")[i].children[5].innerText
        let kamu=document.querySelectorAll(".result-row")[i].children
        if(nama.toLowerCase().trim()!="confirmed"){
          kamen[shipment_header[0]]=kamu[0].innerText
          kamen[shipment_header[1]]=kamu[1].innerText
          kamen[shipment_header[2]]=kamu[2].innerText
          kamen[shipment_header[3]]=kamu[3].innerText
          kamen[shipment_header[4]]=kamu[4].innerText
          kamen[shipment_header[5]]=kamu[5].innerText
          kamen[shipment_header[6]]=kamu[6].innerText
          kamen[shipment_header[7]]=kamu[7].innerText
          
      heder.push(kamen)
        }
      }
      return heder
    })
    objek.booking_information=booking_info
    objek.shipment_status=shipment_info
     var datastore=require('./nyoba')
    var parampa=await datastore(objek.awb,objek)
    console.log(JSON.stringify(parampa))
    await browser.close();
    return objek
    
  }catch(err){
    await browser.close()
  return {error:"error your tracking number not recognizable"}
    
  }
  
}

module.exports=siacargo